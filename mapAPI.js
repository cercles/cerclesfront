let map;
let markers;
let projEPSG4326 = new OpenLayers.Projection("EPSG:4326"); // transformation de WGS 1984
let projEPSG900913 = new OpenLayers.Projection("EPSG:900913"); // en projection Mercator sphérique

let iconSize = new OpenLayers.Size(25, 40);
let iconOffset = new OpenLayers.Pixel(-(iconSize.w / 2), -iconSize.h);
let icon = new OpenLayers.Icon("images/pointer.png",
    iconSize, iconOffset);
let meetingIcon = new OpenLayers.Icon("images/meetingPointer.png",
    iconSize, iconOffset);

let zoom = 6, center, currentPopup, lyrMarkers;
let popupClass = OpenLayers.Class(OpenLayers.Popup.FramedCloud, {
    "autoSize": true,
    "minSize": new OpenLayers.Size(300, 50),
    "maxSize": new OpenLayers.Size(500, 300),
    "keepInMap": true
});

let bounds = new OpenLayers.Bounds();

// let apiurl = 'http://localhost:8000/';
let apiurl = 'http://cerclesapi.jokeur.fr/';

let cercleModel = {
    "id": null,
    "parent": null,
    "coords": null,
    "password":  null,
    "contact":  null,
    "nom": null,
    "inscription": null,
    "doc": null,
    "forum": null,
    "nextMeeting": null
};
let cercles;
let currentCercle = {};
let action;

function setMapCenter(lat, lng, requiredZoom = zoom) {
    map.setCenter(new OpenLayers.LonLat(lng, lat) // Centre de la carte
        .transform(
            projEPSG4326,
            projEPSG900913
        ), requiredZoom // Zoom level
    )
}

function init() {
    getCercles();
}

function manageEvent() {
    $('#create').click((event) => {
        currentCercle = JSON.parse(JSON.stringify(cercleModel));
        openForm()
    });

    map.events.register("click", map, function(e) {
        let lonlat = map.getLonLatFromViewPortPx(e.xy);

        lonlat.transform(
            projEPSG900913,
            projEPSG4326
        );
        console.log(lonlat);
        setCoords(lonlat);
    });
}

function setCoords(lonlat) {
    let coords = {
        type: 'point',
        coordinates: [
            lonlat.lat,
            lonlat.lon
        ]
    };
    console.log(action);
    switch (action) {
        case 'coords':
            currentCercle.coords = coords;
            $('#recapCoord').html('<span>lat : ' + lonlat.lat + ' lon : ' + lonlat.lon + '</span>');
            break;
        case 'meeting':
            if (!currentCercle.nextMeeting) {
                currentCercle.nextMeeting = {
                    date: null,
                    adresse: null,
                    coords: null
                }
            }
            currentCercle.nextMeeting.coords = coords;
            $('#recapMeetingCoord').html('<span>lat : ' + lonlat.lat + ' lon : ' + lonlat.lon + '</span>');
            break;
    }
    action = null;
}

function createMap() {
    map = new OpenLayers.Map("basicMap");
    let mapnik = new OpenLayers.Layer.OSM();
    map.addLayer(mapnik);
    map.addControl(new OpenLayers.Control.DragPan());
    let lyrOsm = new OpenLayers.Layer.OSM();
    map.addLayer(lyrOsm);


    lyrMarkers = new OpenLayers.Layer.Markers("Markers");
    map.addLayer(lyrMarkers);

    setMapCenter(47.10, 2.58);

    for (let cercle of cercles) {
        addMarker(cercle);
    }
}

function getCercles() {
    $.get(apiurl + 'cercles', (_cercles) => {
        cercles = _cercles;
    }).fail(function (error) {
        cercles = [];
        alert('Désolé, les cercles n\'ont pas pu être mis à jour...');
    }).always(function() {
        createMap();
        manageEvent();
    });
}

function addMarker(cercle) {
    let pt = new OpenLayers.LonLat(cercle.coords.coordinates[1], cercle.coords.coordinates[0])
        .transform(
            projEPSG4326,
            projEPSG900913
        );
    bounds.extend(pt);
    let feature = new OpenLayers.Feature(lyrMarkers, pt);
    feature.closeBox = true;
    feature.popupClass = popupClass;
    feature.data.popupContentHTML = getMarkerInfoHtml(cercle) ;
    feature.data.overflow = "auto";
    let marker = new OpenLayers.Marker(pt, icon.clone());

    let markerClick = function(evt) {
        if (currentPopup != null && currentPopup.visible()) {
            currentPopup.hide();
        }

        if (this.popup == null) {
            this.popup = this.createPopup(this.closeBox);
            map.addPopup(this.popup);
            this.popup.show();
            popupEvent(cercle);
        } else {
            this.popup.toggle();
        }
        currentPopup = this.popup;
        OpenLayers.Event.stop(evt);
    };
    marker.events.register("mousedown", feature, markerClick);
    lyrMarkers.addMarker(marker);
}

function addMeetingMarker(cercle) {
    let pt = new OpenLayers.LonLat(cercle.nextMeeting.coords.coordinates[1], cercle.nextMeeting.coords.coordinates[0])
        .transform(
            projEPSG4326,
            projEPSG900913
        );
    bounds.extend(pt);
    let marker = new OpenLayers.Marker(pt, meetingIcon.clone());
    marker.id = 'meeting' + cercle.id;
    lyrMarkers.addMarker(marker);
}

function getMarkerInfoHtml(cercle) {
    let html = '<h1>' + cercle.nom + '</h1>';
    if (cercle.contact) {
        html += '<div>Contact : ' + cercle.contact + '</div>';
    }
    if (cercle.parent) {
        html += '<div>Cercle parent : ' + cercle.parent.nom;
        html += ' (<a class="mapGoto" id="parent_link_' + cercle.parent.id + '">Voir sur la carte</a>)</div>';
    }
    if (cercle.inscription) {
        html += '<div>Inscription : <a href="' + cercle.inscription + '">ici</a></div>';
    }
    if (cercle.doc) {
        html += '<div>Documentation : <a href="' + cercle.doc + '">ici</a></div>';
    }
    if (cercle.forum) {
        html += '<div>Forum : <a href="' + cercle.forum + '">ici</a></div>';
    }
    if (cercle.nextMeeting) {
        html += '<div>Prochaine réunion le : ' + cercle.nextMeeting.date;
        html += ' à l\'adresse suivante : ' + cercle.nextMeeting.adresse;
        html += ' (<a class="mapGoto" id="meeting_link_' + cercle.id +'" >Voir sur la carte</a>)</div>';
    }
    let classParent = '';
    if(action !== 'parent') {
        classParent = ' class="hidden"';
    }
    html += '<div><button id="setParent_' + cercle.id + '"' + classParent + '>Choisir en tant que parent</button></div>';
    html += '<div><button id="edit_' + cercle.id + '">Editer</button></div>';
    return html;
}

function popupEvent(cercle) {
    $('#setParent_' + cercle.id).click(function (event) {
       currentCercle.parent = {
           "id": cercle.id,
           "nom": cercle.nom,
           "coords": cercle.coords
       };
       action = null;
       $('#nomParent').html('<span>' + cercle.nom + '</span>');
       this.addClass('hidden');
    });
    $('#edit_' + cercle.id).click(function (event) {
        currentCercle = cercles.find(c => c.id === cercle.id);
        openForm();
    });
    if (cercle.parent) {
        $('#parent_link_' + cercle.parent.id).click((event) => {
            event.preventDefault();
            let parent = cercles.find(c => c.id === cercle.parent.id);
            console.log('go to parent place');
            setMapCenter(parent.coords.coordinates[0], parent.coords.coordinates[1], 15);
        })
    }
    if (cercle.nextMeeting) {
        $('#meeting_link_' + cercle.id).click((event) => {
            event.preventDefault();
            console.log('go to meeting place');
            currentPopup.hide();

            if (lyrMarkers.markers.find(m => m.id === 'meeting' + cercle.id) === undefined) {
                addMeetingMarker(cercle);
            }
            setMapCenter(cercle.nextMeeting.coords.coordinates[0], cercle.nextMeeting.coords.coordinates[1], 16);
        })
    }
}

function openForm () {
    // set values
    $('#contact').val(currentCercle.contact);
    $('#nom').val(currentCercle.nom);
    $('#password').val('');
    $('#inscription').val(currentCercle.inscription);
    $('#doc').val(currentCercle.doc);
    $('#forum').val(currentCercle.forum);
    if (currentCercle.coords) {
        $('#recapCoord').html('<span>lat : ' + currentCercle.coords.coordinates[0] + ' lon : ' + currentCercle.coords.coordinates[1] + '</span>');
    } else {
        $('#recapCoord').html('');
    }
    if(currentCercle.nextMeeting && currentCercle.nextMeeting.coords != null) {
        $('#date').val(currentCercle.nextMeeting.date);
        $('#adresse').val(currentCercle.nextMeeting.adresse);
        $('#recapMeetingCoord').html('<span>lat : ' + currentCercle.nextMeeting.coords.coordinates[0] + ' lon : ' + currentCercle.nextMeeting.coords.coordinates[1] + '</span>');
    } else {
        $('#date').val('');
        $('#adresse').val('');
        $('#recapMeetingCoord').html('');

    }
    if(currentCercle.parent) {
        $('#nomParent').html('<span>' + currentCercle.parent.nom + '</span>');
    } else {
        $('#nomParent').html('');
    }

    // open form div and set event
    $('#basicMap').addClass('small');
    $('#cercleForm').removeClass('hidden');
    // add event for form buttons
    $('#coords').click((event) => {
        action = 'coords';
    });
    $('#parent').click((event) => {
        action = 'parent';
        $('#setParent').removeClass('hidden');
    });
    $('#meetingcoords').click((event) => {
        action = 'meeting';
    });
    $('#cancel').click((event) => {
        closeForm();
    });
    $('#delete').click((event) => {
        deleteCurrent();
    });
}

function closeForm () {
    // set values
    $('#contact').val('');
    $('#nom').val('');
    $('#password').val('');
    $('#inscription').val('');
    $('#doc').val('');
    $('#forum').val('');
    $('#recapCoord').html('');
    $('#date').val('');
    $('#adresse').val('');
    $('#recapMeetingCoord').html('');
    $('#nomParent').html('');
    // close form div
    $('#basicMap').removeClass('small');
    $('#cercleForm').addClass('hidden');
}

let formError = '';
function save() {
    if (formIsValid()) {
        currentCercle.password = $('#password').val();
        currentCercle.contact = $('#contact').val();
        currentCercle.nom = $('#nom').val();
        currentCercle.inscription = $('#inscription').val();
        currentCercle.doc = $('#doc').val();
        currentCercle.forum = $('#forum').val();

        let adresse = $('#adresse');
        let date = $('#date');
        if (date.val() !== '' || adresse.val() || currentCercle.nextMeeting.coords != null) {
            // si un des champs de l'AG est défini tous doivent etre définis
            currentCercle.nextMeeting.date = date.val();
            currentCercle.nextMeeting.adresse = adresse.val();
        }
        console.log(currentCercle);
        if (currentCercle.id == null) {
            console.log('post');
            // si pas d'id : post
            $.post(apiurl + 'cercles/', JSON.stringify(currentCercle), function(retour) {
                console.log(retour);
                alert('Cercle enregistré avec succès');
                closeForm();
                getCercles();
            }).fail(function (error) {
                alert('Le cercles n\'a pas pu être créé.');
            });
        } else {
            console.log('put');
            // else : put
            $.ajax({
                type: "PUT",
                url: apiurl + 'cercles/',
                contentType: "application/json",
                data: JSON.stringify(currentCercle),
                success: function(retour) {
                    console.log(retour);
                    alert('Cercle enregistré avec succès');
                    closeForm();
                    getCercles();
                },
                error: function (error) {
                    alert('Le cercles n\'a pas pu être mis à jour.');
                    console.log(error);
                }
            });
        }
    } else {
        alert(formError);
        formError = '';
    }
}

function formIsValid() {
    let valid = true;
    // le nom est obligatoire
    valid = valid && $('#nom').val() !== '';
    if (!valid) {
        formError += '<div>Le nom est obligatoire</div>'
    }
    // le nom est obligatoire
    valid = valid && $('#password').val() !== '';
    if (!valid) {
        formError += '<div>Le mot de passe est obligatoire</div>'
    }
    // des coordonnées sont obligatoires
    valid = valid && currentCercle.coords !== null;
    valid = valid && typeof currentCercle.coords.coordinates[0] === "number";
    valid = valid && typeof currentCercle.coords.coordinates[1] === "number";
    if (!valid) {
        formError += '<div>Les coordonnées du cercle sont inexistantes ou incorrectes</div>'
    }

    if (currentCercle.parent != null) {
        // si un parent est défini il doit avoir un nom un id et des coords
        valid = valid && currentCercle.parent.nom !== null && currentCercle.parent.nom !== '';
        valid = valid && currentCercle.parent.id !== null && currentCercle.parent.id !== '';
        valid = valid && currentCercle.parent.coords !== null;
        valid = valid && typeof currentCercle.parent.coords.coordinates[0] === "number";
        valid = valid && typeof currentCercle.parent.coords.coordinates[1] === "number";
        if (!valid) {
            formError += '<div>Le parent sélectionné est incorrecte</div>'
        }
    }

    let adresse = $('#adresse');
    let date = $('#date');
    if (date.val() !== '' || adresse.val() || currentCercle.nextMeeting.coords != null) {
        // si un des champs de l'AG est défini tous doivent etre définis
        valid = valid && date.val() !== '';
        valid = valid && adresse.val() !== '';
        valid = valid && currentCercle.nextMeeting.coords !== null;
        valid = valid && typeof currentCercle.nextMeeting.coords.coordinates[0] === "number";
        valid = valid && typeof currentCercle.nextMeeting.coords.coordinates[1] === "number";
        if (!valid) {
            formError += '<div>Tous les champs sont obligatoire pour définir une AG</div>'
        }
    }

    return valid;
}

function deleteCurrent() {
    currentCercle.password = $('#password').val();
    if (!currentCercle.password) {
        alert('Le mot de passe est obligatoire');
    } else {
        $.ajax({
            type: "DELETE",
            url: apiurl + 'cercles/',
            contentType: "application/json",
            data: JSON.stringify(currentCercle),
            success: function(retour) {
                console.log(retour);
                alert('Cercle supprimé avec succès');
                closeForm();
                getCercles();
            },
            error: function (error) {
                alert('Une erreur est survenue');
                console.log(error);
            }
        });
    }
}
